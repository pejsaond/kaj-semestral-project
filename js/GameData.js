class GameData
{
    constructor()
    {
        this._nickname = null;
        this._pressedKeys = {};
        this._pressedButton = false;
        //hud
        this.gameBar = null;
        this.scoreBar = null;
        this.gameWindow = null;
        //game objects
        this._gameObjects = {tanks: [],bullets: [], cannons: [],zones: []};
    }

    setNickname(nickname)
    {
        this._nickname = nickname;
    }

    getNickname()
    {
        return this._nickname;
    }

    isPressedKey(key)
    {
        if(typeof(this._pressedKeys[key]) === "undefined")
            return false;
        return this._pressedKeys[key];
    }

    getPressedKeys()
    {
        return this._pressedKeys;
    }

    prepareKeyEvents()
    {
        var keyDown = function(event)
        {
            this._pressedKeys[event.key] = true;
        }.bind(this);

        var keyUp = function(event)
        {
            this._pressedKeys[event.key] = false;
        }.bind(this);

        window.addEventListener("keydown",keyDown);
        window.addEventListener("keyup",keyUp);
    }

    createHUD()
    {
        this.hpBar = new HpBar(document.querySelector(".game-center"));
        this.scoreBar = new ScoreBar(document.querySelector(".game-center"));
        this.gameWindow = new GameWindow(document.querySelector(".game-center"));

    }

    record()
    {
        var storage = window.localStorage;
        var rec = JSON.stringify({"nick":this.scoreBar.scoreElement.textContent, "date": new Date().toLocaleDateString()});
        storage.setItem(this._nickname,rec);
    }

    //game objects -------------
    
    getTanks()
    {
        return this._gameObjects.tanks;
    }

    getBullets()
    {
        return this._gameObjects.bullets;
    }

    getCannons()
    {
        return this._gameObjects.cannons;
    }

    getZones()
    {
        return this._gameObjects.zones;
    }

    //return gameObjects in order by arguments
    getGameObjects(...order)
    {
        var gameObjectsArray = [];
        for(var i = 0;i < order.length;i++)
            gameObjectsArray.push(this._gameObjects[order[i]]);
        return gameObjectsArray;
    }

    clearObjects()
    {
        for(let key in this._gameObjects)
            this._gameObjects[key] = [];
    }
}

