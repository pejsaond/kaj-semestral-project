class Environment
{
    constructor(basic,builder = null)
    {
        this._basic = basic;
        this._builder = builder;
        this._gameData = null;
    }

    build()
    {
        this._builder.build(this);
        this._builder.exec(this);
        gameData.module = this._builder;
    }

    clear()
    {
        this.getBasic().innerHTML = "";
    }

    setModule(builder)
    {
        this._builder = builder;
    }

    getBasic()
    {
        return this._basic;
    }

    setGameData(gameData)
    {
        this._gameData = gameData;
    }

    getGameData()
    {
        return this._gameData;
    }   
}
