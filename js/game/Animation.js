class Animation
{
    constructor(images) //array of images to drawing
    {
        this._images = images;
        this._currentImageIndex = 0;
        this._fixDur = 1;
        this._currentDur = 0;
        this._repeat = true;
    }

    draw(canvasCtx,coord)
    {
        var img = this.getCurrentImage();
        if(img.complete)
            canvasCtx.drawImage(img,coord.x,coord.y);
        else
            img.onload= () => {canvasCtx.drawImage(img,coord.x,coord.y)};
        this._currentDur++;
    }

    nextImage()
    {
        if(this._currentDur < this._fixDur)
            return;
        if(!(this._repeat == false && this._currentImageIndex == this._images.length-1))
            this._currentDur = 0;
        if((this._currentImageIndex+1) < this._images.length)
            this._currentImageIndex++;
        else if(this._repeat == true)
            this._currentImageIndex = 0;
    }

    getCurrentImage()
    {
        return this._images[this._currentImageIndex];
    }

    isFinishedAnimation()
    {
        return (this._currentImageIndex == (this._images.length-1)) && (this._repeat == false) && (this._currentDur >= this._fixDur);
    }

    setDuration(duration)
    {
        return this._fixDur = duration;
    }

    getDuration()
    {
        return this._fixDur;
    }

    resetCurrentDur()
    {
        this._currentDur = 0;
    }

    setRepeat(repeat)
    {
        return this._repeat = repeat;
    }

    getRepeat()
    {
        return this._repeat;
    }

    setCurrentImageIndex(index)
    {
        this._currentImageIndex = index;
    }

    getSize()
    {
        return this._size;
    }

    setSize(size)
    {
        this._size = size;
    }
}



