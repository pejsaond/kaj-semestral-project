class Basic
{
    constructor()
    {
        this._position = new Position();
        this.collision = new Collision(this);
        this._currentAnimation = null;
        this._basicChilds = {};
        this._active = true;
        this.damaged = false;
    }

    draw(canvasCtx)
    {
        canvasCtx.save();
        this._applyTransform(canvasCtx);
        for(var key in this._basicChilds) //draw childs
        {
            if(this._basicChilds[key].getActive())
            {
                this._basicChilds[key].draw(canvasCtx);
                this._basicChilds[key].getAnimation().nextImage();
            }        
        }
        this._currentAnimation.draw(canvasCtx,this._position.getCoord());
        this._currentAnimation.nextImage();
        var t = canvasCtx.getTransform();
        canvasCtx.restore();
        this.collision.draw(canvasCtx,this._position.getTransform());
    }

    setPosition(position)
    {
        this._position = position;
    }

    getPosition()
    {
        return this._position;
    }

    getAnimation()
    {
        return this._currentAnimation;
    }

    getActive()
    {
        return this._active;
    }

    setActive(active)
    {
        return this._active = active;
    }

    //set center of current image to 0,0
    setCenterToRoot()
    {
        var img = this._currentAnimation.getCurrentImage();
        if(img.complete)
        {
            this._position.getCoord().x = 0 - (this._currentAnimation.getCurrentImage().width/2);
            this._position.getCoord().y = 0 - (this._currentAnimation.getCurrentImage().height/2);
        }
        else
        {
            img.onload = function(){
                this._position.getCoord().x = 0 - (this._currentAnimation.getCurrentImage().width/2);
                this._position.getCoord().y = 0 - (this._currentAnimation.getCurrentImage().height/2);
            }.bind(this);
        }
    }

    setCenterToPosition(x,y)
    {
        this._position.getCoord().x = x;
        this._position.getCoord().y = y;
    }

    _applyTransform(canvasCtx)
    {
        var allMatrixes = this._position.allMatrixes();
        for(var i = (allMatrixes.length-1);i >= 0;i--)
        {
            var metrix = allMatrixes[i];
            canvasCtx.transform(metrix.a,metrix.b,metrix.c,metrix.d,metrix.e,metrix.f);
        }
    }

    collisionProcess()
    {
        //nothing
    }
}