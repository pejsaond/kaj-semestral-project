class Bullet extends Basic
{
    constructor(images)
    {
        super();
        this._currentAnimation = new Animation(images);
        this._currentAnimation.setDuration(100);
        this._velocity = 10;
        this._range = 800;
        //collision shape
        this.collision.setShapes([{x: -5,y: 5},{x: 5,y: 5},{x: 5,y: -5},{x: -5,y: -5}]);  
    }

    move()
    {
        if(this._range < 0)
            this.setActive(false);
        this._range -= this._velocity;
        var angle = Position.getAngleFromMatrix(this._position.allMatrixes()[0]);
        angle = Position.getRadians(angle);
        var matrice = this._position.allMatrixes()[1];
        var move = Position.createMoveMatrix(this._velocity*Math.sin(angle),this._velocity*Math.cos(angle));
        this._position.replace(1,Position.multiplyMatrix(matrice,move));
    }

    gameLoopProcess(ctx)
    {
        this.draw(ctx);
        this.move();
        this.collision.check();
    }

    events()
    {
        //nothing
    }

    collisionProcess()
    {
        var d = gameData.scoreBar.getDestroyed();
        var s = gameData.scoreBar.getScore();
        gameData.scoreBar.setDestroyed(d + 1);
        gameData.scoreBar.setScore(s + 2);
        this.setActive(false);
    }
}