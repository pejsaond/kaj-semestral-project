class Cannon extends Basic
{
    constructor(human)
    {
        super();
        var cannon_images = [];
        if(imageCache.has("images/game/cannon/cannon0.png"))
            cannon_images = imageCache.get("images/game/cannon/cannon0.png");
        else
        {
            for(var i = 0;i < 1;i++)
            {
                cannon_images.push(new Image());
                cannon_images[i].src = "images/game/cannon/cannon" + i + ".png";
            }
            imageCache.set("images/game/cannon/cannon0.png",cannon_images)
        }
        this._anim = new Animation(cannon_images);
        this._currentAnimation = this._anim;
        this._position.pushMatrix(Position.createRotateMatrix(0));
        this._position.pushMatrix(Position.createMoveMatrix(0,0));
        this._rotation = 3;
        this._human = human;
        //shooting
        this._shooting = new Shooting(this,280,0);
        this.shotAudio = new Audio("sound/shot.mp3");
    }

    draw(ctx)
    {
        super.draw(ctx);
    }

    rotateClockWise()
    {
        var matrice = this._position.allMatrixes()[0];
        var rotation = Position.createRotateMatrix(this._rotation);
        this._position.replace(0,Position.multiplyMatrix(matrice,rotation));
    }

    rotateAntiClockWise()
    {
        var matrice = this._position.allMatrixes()[0];
        var rotation = Position.createRotateMatrix(-this._rotation);
        this._position.replace(0,Position.multiplyMatrix(matrice,rotation));
    }

    shot()
    {
        this._shooting.shot();
    }

    getBullet()
    {
        return this._shooting.getBullet();
    }

    gameLoopProcess(ctx)
    {
        ctx.save();
        this.draw(ctx);
        ctx.restore();
    }

    events()
    {
        if(!this._human)
            return;
        var bullets = gameData.getBullets();
        //check events
        if(gameData.isPressedKey("a"))
        {
            this.rotateAntiClockWise();
            this.getAnimation().nextImage();
        }
        if(gameData.isPressedKey("d"))
        {
            this.rotateClockWise();
            this.getAnimation().nextImage();
        }
        if(gameData.isPressedKey(" ") && this._shotAllowed)
        {
            if(this.shotAudio.duration == 0 || this.shotAudio.paused)
                this.shotAudio.play();
            this.shotAudio.currentTime = 0;
            this._shotAllowed = false;
            this.shot();
            var bullet = this.getBullet();
            bullet.collision.attachList(gameData.getTanks());
            bullets.push(bullet);
            var s = gameData.scoreBar.getShots();
            var sc = gameData.scoreBar.getScore();
            gameData.scoreBar.setShots(s+1);
            gameData.scoreBar.setScore(sc-1);
        }

        if(!gameData.isPressedKey(" "))
            this._shotAllowed = true;
    }
}
