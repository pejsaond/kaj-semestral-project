class Collision
{
    constructor(collisionObject)
    {
        this.collisionObject = collisionObject;
        this._attachedObjs = [];
        this._color = 'green';
    }

    attach(attachObject)
    {
        this._attachedObjs.push(attachObject);
    }

    attachList(attachObjects)
    {
        for(var obj of attachObjects)
            this.attach(obj.collision);
    }

    setShapes(shapes)
    {
        this._shape = shapes;
    }

    check()
    {
        for(var attachedObj of this._attachedObjs)
        {
            if(attachedObj.isCollision(this))
                attachedObj.collisionWith(this); 
        }
    }

    isCollision(collision)
    {
        var opponent = collision.collisionObject;
        var p = opponent.getPosition();
        var opponentShape = collision.getCurrentShape(p.getTransform());
        var myShape = this.getCurrentShape(this.collisionObject._position.getTransform());
        var oponentInterval = this.getInterval(this.getPrimitiveRect(opponentShape));
        var myInteval = this.getInterval(this.getPrimitiveRect(myShape));
        if(myInteval.xmin < oponentInterval.xmax && myInteval.xmax > oponentInterval.xmin &&
            myInteval.ymin < oponentInterval.ymax && myInteval.ymax > oponentInterval.ymin)
            return true;
        return false;
    }

    getCurrentShape(matrix)
    {
        var currentShape = [];
        for(var point of this._shape)
            currentShape.push(this.getPoint(matrix,{a: 0,b: 0,c: 0,d: 0,e: point.x,f: point.y}));
        return currentShape;
    }

    getPoint(matrix,point)
    {
        var tPoint = Position.multiplyMatrix(matrix,point);
        return {x: tPoint.e,y: tPoint.f};
    }

    collisionWith(collision)
    {
        collision.collisionObject.collisionProcess();
        this.collisionObject.collisionProcess();
    }

    getPrimitiveRect(shape)
    {
        var xmax = -Infinity, xmin = Infinity;
        var ymax = -Infinity, ymin = Infinity;
        for(var point of shape)
        {
            if(point.x > xmax)
                xmax = point.x;
            if(point.x < xmin)
                xmin = point.x;
            if(point.y > ymax)
                ymax = point.y;
            if(point.y < ymin)
                ymin = point.y;
        }
        return [{x: xmin,y: ymax},{x: xmax,y: ymax},{x: xmax,y: ymin},{x: xmin,y: ymin}];
    }

    getInterval(shape) //for getPrimitiveRect
    {
        return {xmin: shape[0].x,xmax: shape[1].x,ymin: shape[2].y,ymax: shape[0].y}
    }

    draw(ctx,matrix) //drawing collision shape - for debbuging
    {
        // DEBBUGING PURPOSE
        // displays collision border

        /*ctx.save();
        //ctx.setTransform(1,0,0,1,0,0);
        ctx.strokeStyle = this._color;
        ctx.lineWidth = 2;
        ctx.beginPath();
        var shape = this.getPrimitiveRect(this.getCurrentShape(matrix));
        for(var i = 0;i < shape.length;i++)
            ctx.lineTo(shape[i].x,shape[i].y);
        ctx.closePath();
        ctx.stroke();
        ctx.restore();*/
    }
}
