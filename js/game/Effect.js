class Effect extends Basic
{
    constructor(images)
    {
        super();
        this._currentAnimation = new Animation(images);
        this._currentAnimation.setDuration(1);
        this._currentAnimation.setRepeat(false);
    }

    draw(canvasCtx)
    {
        if(this._currentAnimation.isFinishedAnimation())
            this._active = false; 
        super.draw(canvasCtx);
    }
}