/*

- Matrix -

array: [a b 0 c d 0 e f 1]

| a c e |
| b d f |
| 0 0 1 |

*/

class Position
{
    static createMoveMatrix(x,y)
    {
        return {a: 1,b: 0,c: 0,d: 1,e: x,f: y};
    }

    static createRotateMatrix(degrees)
    {
        var radians = Position.getRadians(degrees);
        var cos = Math.cos(radians);
        var sin = Math.sin(radians);
        return {a: cos,b: sin,c: -sin,d: cos,e: 0,f: 0};
    }

    static multiplyMatrix(matrixA,matrixB, ...matrixes)
    {
        var context = document.createElement("canvas").getContext("2d");
        context.setTransform(matrixA);
        context.transform(matrixB.a,matrixB.b,matrixB.c,matrixB.d,matrixB.e,matrixB.f);
        for(var i = 0;i < matrixes.length;i++)
            context.transform(matrixes[i].a,matrixes[i].b,matrixes[i].c,matrixes[i].d,matrixes[i].e,matrixes[i].f);
        return context.getTransform();
    }

    static getRadians(degrees)
    {
        return (degrees*Math.PI)/180;
    }

    static getDegrees(radians)
    {
        return (radians*180)/Math.PI;
    }

    static getAngleFromMatrix(matrix)
    {
        return Position.getDegrees(Math.atan2(matrix.a,matrix.b));
    }

    constructor()
    {
        this._coord = {x: 0,y: 0};
        this._transformation = []
    }

    getCoord()
    {
        return this._coord;
    }

    setCoord(coord)
    {
        this._coord = coord;
    }

    setTransformation(transformation)
    {
        this._transformation = transformation
    }

    pushMatrix(matrix)
    {
        this._transformation.push(matrix);
    }

    popMatrix()
    {
        return this._transformation.pop(0);
    }

    allMatrixes()
    {
        return this._transformation;
    }

    pushValues(a,b,c,d,e,f)
    {
        this.pushMatrix({"a": a,"b": b,"c": c,"d": d,"e": e,"f": f});
    }

    replace(index,matrix)
    {
        this._transformation[index] = matrix;
    }

    getCopyTransformation()
    {
        var transformation = []
        for(var i = 0;i < this._transformation.length;i++)
        {
            var mat = this._transformation[i];
            transformation.push({"a": mat.a,"b": mat.b,"c": mat.c,"d": mat.d,"e": mat.e,"f": mat.f});
        }
        return transformation;
    }

    getTransform()
    {
        var allMatrixes = this.allMatrixes();
        if(allMatrixes.length > 1)
            return Position.multiplyMatrix(...allMatrixes.slice().reverse());
        return allMatrixes[0];
    }
}

