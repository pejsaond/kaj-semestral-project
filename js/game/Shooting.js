class Shooting
{
    constructor(shootingObject,x,y)
    {
        this.shootingObject = shootingObject;
        this.x = x;
        this.y = y;
        var shot_images = [];
        if(imageCache.has("images/game/effect/shot0.png"))
            shot_images = imageCache.get("images/game/effect/shot0.png");
        else
        {
            for(var i = 0;i < 7;i++)
            {
                shot_images.push(new Image());
                shot_images[i].src = "images/game/effect/shot" + i + ".png"; //shot
            }
            imageCache.set("images/game/effect/shot0.png",shot_images);
        }
        this.shootingObject._basicChilds.shot = new Effect(shot_images);
        this.shootingObject._basicChilds.shot.setCenterToRoot();
        this.shootingObject._basicChilds.shot.getPosition().pushMatrix(Position.createMoveMatrix(x,y));
        this.shootingObject._basicChilds.shot.setActive(false);
        this._lastPoint = 0;
        this._shotAllowed = true;
    }

    shot()
    {
        this.shootingObject._basicChilds.shot.getAnimation().resetCurrentDur();
        this.shootingObject._basicChilds.shot.getAnimation().setCurrentImageIndex(0);
        this.shootingObject._basicChilds.shot.setActive(true);
    }

    getBullet()
    {
        if(imageCache.has("images/game/tank/bullets/bullet.png"))
            var imgBullet = imageCache.get("images/game/tank/bullets/bullet.png");
        else
        {
            var imgBullet = [new Image()];
            imgBullet[0].src = "images/game/tank/bullets/bullet.png";
            imageCache.set("images/game/tank/bullets/bullet.png",imgBullet);
        } 
        var bullet = new Bullet(imgBullet,this.shootingObject.player);
        var trans = this.shootingObject._position.getCopyTransformation()
        trans.unshift(Position.createMoveMatrix(this.x,this.y));
        bullet.getPosition().setTransformation(trans);
        bullet.setCenterToRoot();
        return bullet;
    }
}
