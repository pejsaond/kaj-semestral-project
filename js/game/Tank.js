class Tank extends Basic
{
    constructor(human)
    {
        super();
        var tank_images = [];
        if(imageCache.has("images/game/tank/tank0.png"))
            tank_images = imageCache.get("images/game/tank/tank0.png"); 
        else
        {
            for(var i = 0;i < 6;i++)
            {
                tank_images.push(new Image());
                tank_images[i].src = "images/game/tank/tank" + i + ".png";
            }
            imageCache.set("images/game/tank/tank0.png",tank_images);
        }
        this._moveAnim = new Animation(tank_images);
        this._currentAnimation = this._moveAnim;
        this._position.pushMatrix(Position.createRotateMatrix(0)); //first matrix for rotation - index 0
        this._position.pushMatrix(Position.createMoveMatrix(100,100)); //second matrix for move - index 1
        //tank properties
        this._velocity = 3;
        this._rotation = 3;
        this._human = human;
        //trajectory
        this._trajectories = [new Trajectory(),new Trajectory()];
        this._lastPoint = 0;
        //shooting
        this._shooting = new Shooting(this,0,0);
        //collision shape
        this.collision.setShapes([{x: -65,y: 30},{x: 30,y: 30},{x: 30,y: -30},{x: -65,y: -30}]);   
    }

    draw(ctx)
    {
        for(var i = 0;i < this._trajectories.length;i++)
            this._trajectories[i].draw(ctx);
        super.draw(ctx);
    }

    move()
    {
        var angle = Position.getAngleFromMatrix(this._position.allMatrixes()[0]);
        angle = Position.getRadians(angle);
        var sin = Math.sin(angle);
        var cos = Math.cos(angle);
        var matrice = this._position.allMatrixes()[1];
        var move = Position.createMoveMatrix(this._velocity*sin,this._velocity*cos);
        this._position.replace(1,Position.multiplyMatrix(matrice,move));
        this._currentAnimation = this._moveAnim;
        if(Date.now() - this._lastPoint > 150)
        {
            this._lastPoint = Date.now();
            this._trajectories[0].addPoint(matrice.e-((sin*50)-(cos*35)),matrice.f-((cos*50)+(sin*35)));
            this._trajectories[1].addPoint(matrice.e-((sin*50)+(cos*35)),matrice.f-((cos*50)-(sin*35)));
        }   
    }

    rotateClockWise()
    {
        var matrice = this._position.allMatrixes()[0];
        var rotation = Position.createRotateMatrix(this._rotation);
        this._position.replace(0,Position.multiplyMatrix(matrice,rotation));
    }

    rotateAntiClockWise()
    {
        var matrice = this._position.allMatrixes()[0];
        var rotation = Position.createRotateMatrix(-this._rotation);
        this._position.replace(0,Position.multiplyMatrix(matrice,rotation));
    }

    shot() //shot
    {
        this._shooting.shot();
    }

    getBullet() //shot
    {
        return this._shooting.getBullet();
    }

    gameLoopProcess(ctx)
    {
        ctx.save();
        this.draw(ctx);
        ctx.restore();
        this.collision.check();
        this.move();
    }

    events()
    {
        if(!this._human)
            return;
        var bullets = gameData.getBullets();
        //check events
        if(gameData.isPressedKey("w"))
        {
            this.move();
            this.getAnimation().nextImage();
        }
        if(gameData.isPressedKey("a"))
        {
            this.rotateAntiClockWise();
            this.getAnimation().nextImage();
        }
        if(gameData.isPressedKey("d"))
        {
            this.rotateClockWise();
            this.getAnimation().nextImage();
        }
        if(gameData.isPressedKey(" ") && this._shotAllowed)
        {
            this._shotAllowed = false;
            this.shot();
            var bullet = this.getBullet();
            bullet.collision.attachList(gameData.getTanks());
            bullets.push(bullet);
        }

        if(!gameData.isPressedKey(" "))
            this._shotAllowed = true;
    }

    collisionProcess()
    {
        this.damaged = true;
    }
}
