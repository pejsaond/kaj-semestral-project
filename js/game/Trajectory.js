class Trajectory
{
    constructor()
    {
        this._track = [];
        this._width = 10;
    }

    addPoint(x,y)
    {
        this._track.unshift({"x": x,"y": y,"alpha": 1});
    }

    draw(ctx)
    {
        if(this._track.length < 2)
            return; // cant draw because line needs at least 2 points
        ctx.beginPath();
        ctx.lineWidth = this._width;
        var point = this._track[0];
        ctx.moveTo(point.x,point.y);
        for(var i = 1;i < this._track.length;i++)
        {
            point = this._track[i]
            ctx.globalAlpha = point.alpha;
            ctx.lineTo(point.x,point.y);
            ctx.stroke();
            if(point.alpha > 0)
                point.alpha -= 0.02;
            else
            {
                point.alpha = 0;
                this._track.splice(i,1);
                i--;
            }  
        }
        ctx.globalAlpha = 1;
    }
}