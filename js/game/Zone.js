class Zone extends Basic
{
    constructor()
    {
        super();
        var zone_images = [];
        if(imageCache.has("images/game/zone/zone0.png"))
            zone_images = imageCache.get("images/game/zone/zone0.png");
        else
        {
            for(var i = 0;i < 1;i++)
            {
                zone_images.push(new Image());
                zone_images[i].src = "images/game/zone/zone" + i + ".png";
            }
            imageCache.set("images/game/zone/zone0.png",zone_images);
        }
        
        this._moveAnim = new Animation(zone_images);
        this._currentAnimation = this._moveAnim;
        this._position.pushMatrix(Position.createRotateMatrix(0)); //first matrix for rotation - index 0
        this._position.pushMatrix(Position.createMoveMatrix(0,0)); //second matrix for move - index 1
        //collision shape
        this.collision.setShapes([{x: -118/2,y: 1000/2},{x: 118/2,y: 1000/2},{x: 118/2,y: -1000/2},{x: -118/2,y: -1000/2}]);
    }

    draw(ctx)
    {
        super.draw(ctx);
    }

    gameLoopProcess(ctx)
    {
        this.draw(ctx);
    }

    events()
    {

    }

    collisionProcess()
    {
        gameData.hpBar.removeLife();
    }
}