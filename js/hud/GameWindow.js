class GameWindow
{
    constructor(element)
    {
        this.element = element;
        this.gameWindowContainer = document.createElement("div");
        this.gameWindowContainer.classList.add("windowContainer");
        this.gameWindowContainer.appendChild(this._createForm());
    }

    _createForm()
    {
        var form = document.createElement("form");
        form.appendChild(this._createButton("START",(e) => {
                e.preventDefault();
                gameData.module.start();
                this.hide();
            }));
        return form;
    }

    _createButton(text,fce)
    {
        var b = document.createElement("button");
        b.classList.add("black");
        b.textContent = text;
        b.addEventListener("click",fce);
        return b;
    }

    show()
    {
        this.element.insertBefore(this.gameWindowContainer,this.element.firstChild);
    }   

    hide()
    {
        this.element.removeChild(this.gameWindowContainer);
    }
}