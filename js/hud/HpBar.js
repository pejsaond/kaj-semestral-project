class HpBar
{
    constructor(element)
    {
        this.svgns = "http://www.w3.org/2000/svg";
        this.hpContainerElement = document.createElement("div");
        this.hpContainerElement.classList.add("hpContainer");
        this.svg = document.createElementNS(this.svgns,"svg");
        this.svg.classList.add("fall");
        this.svg.setAttribute("height",55);
        this.svg.setAttribute("width",170);
        this.hpContainerElement.appendChild(this._createTitleElement());
        this.hpContainerElement.appendChild(this.svg);
        this.hpContainerElement.classList.add("game-hud");
        this.hpContainerElement.classList.add("fall");
        element.appendChild(this.hpContainerElement);
        this.life = 0;
    }

    addLifes(n)
    {
        for(var i = 0;i < n;i++)
        {
            var shape = document.createElementNS(this.svgns,"circle");
            shape.setAttribute("cx",26+(55*this.life));
            shape.setAttribute("cy",26);
            shape.setAttribute("r",25);
            shape.setAttribute("stroke","black")
            shape.setAttribute("stroke-width",2);
            shape.setAttribute("fill","rgb(150,0,0)");
            shape.setAttribute("fill-opacity",1 - (0.3*this.life));
            this.svg.appendChild(shape);
            this.life++;
        }
    }

    removeLife()
    {
        if(this.life > 0)
        {
            this.life--;
            var cx = (26+(55*this.life));
            var shape = this.svg.querySelector("circle[cx='" + cx + "']");
            this.svg.removeChild(shape);
        }
        if(this.life == 0)
        {
            window.clearInterval(gameData.module.loop);
            gameData.gameWindow.show();
            gameData.record();
        }
    }

    removeAllLifes()
    {
        this.life = 0;
        this.svg.innerHTML = "";
    }

    _createTitleElement()
    {
        var title = document.createElement("h1");
        title.innerText = "HP";
        return title;
    }
}