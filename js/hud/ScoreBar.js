class ScoreBar
{
    constructor(element)
    {
        this.scoreContainerElement = document.createElement("div");
        this.scoreContainerElement.classList.add("scoreContainer");
        this.scoreContainerElement.classList.add("game-hud");
        this.scoreContainerElement.classList.add("fall");

        this._createDestroyed();
        this._createShots();
        this._createScore();
        this.reset();

        element.appendChild(this.scoreContainerElement);
    }

    getDestroyed()
    {
        return Number(this.destroyedElement.textContent);
    }

    setDestroyed(number)
    {
        this.destroyedElement.textContent = number;
    }

    getShots()
    {
        return Number(this.shotsElement.textContent);
    }

    setShots(number)
    {
        this.shotsElement.textContent = number;
    }

    getScore()
    {
        return Number(this.scoreElement.textContent);
    }

    setScore(number)
    {
        this.scoreElement.textContent = number;
    }

    reset()
    {
        this.setDestroyed(0);
        this.setShots(0);
        this.setScore(0);
    }

    _createDestroyed()
    {
        var destroyedDiv = document.createElement("div");
        this.destroyedElement = document.createElement("h1");
        var destroyedText = document.createElement("h1");
        destroyedText.innerText = "Destroyed: ";
        destroyedDiv.appendChild(destroyedText);
        destroyedDiv.appendChild(this.destroyedElement);
        this.scoreContainerElement.appendChild(destroyedDiv);
    }

    _createShots()
    {
        var shotsDiv = document.createElement("div");
        this.shotsElement = document.createElement("h1");
        var shotsText = document.createElement("h1");
        shotsText.innerText = "Shots: ";
        shotsDiv.appendChild(shotsText);
        shotsDiv.appendChild(this.shotsElement);
        this.scoreContainerElement.appendChild(shotsDiv);
    }

    _createScore()
    {
        var scoreDiv = document.createElement("div");
        this.scoreElement = document.createElement("h1");
        var scoreText = document.createElement("h1");
        scoreText.innerText = "Score: ";
        scoreDiv.appendChild(scoreText);
        scoreDiv.appendChild(this.scoreElement);
        this.scoreContainerElement.appendChild(scoreDiv);
    }
}