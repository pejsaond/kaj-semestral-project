class Game
{
    constructor()
    {
        this.canvas = null;
        this._shotAllowed = true;
        this._lastTime = 0;
        this.speed = 1;
        this.speedTime = 0;
        this.canvasWidth = 1280;
        this.canvasHeight = 800;
    }

    build(environment)
    {
        this.environment = environment;
        this._buildGameCenterAndCanvas(environment);

        //create hud and prepare events
        gameData.prepareKeyEvents();
        gameData.createHUD();
        gameData.hpBar.addLifes(3);
    }

    exec(environment)
    {
        //start game loop, invokes every 30 miliseconds
        gameData.gameWindow.show();
    }

    start()
    {
        gameData.clearObjects();

        //set cannon
        var cannon = new Cannon(true,gameData);
        cannon.setCenterToPosition(-56,-117/2);
        cannon._position.replace(1,Position.createMoveMatrix(0,this.canvasHeight/2));
        gameData.getCannons().push(cannon);
        
        //set zone
        var zone = new Zone();
        zone.setCenterToRoot();
        zone._position.replace(1,Position.createMoveMatrix(118/2,this.canvasHeight/2));
        gameData.getZones().push(zone);

        this.loop = setInterval(this._gameLoop.bind(this),23);
        gameData.hpBar.removeAllLifes();
        gameData.hpBar.addLifes(3);
        gameData.scoreBar.reset();
        gameData.gameWindow.show();
        this.gameObjects = gameData.getGameObjects("zones","bullets","tanks","cannons");

        this.speed = 1;
        this._intervalNewTank = 2800;
    }

    end()
    {
        this.environment.setModule(new Welcome());
        environment.clear();
        environment.build();
        gameData.hpBar.removeAllLifes();
    }

    _buildGameCenterAndCanvas(environment)
    {
        //creates and sets canvas element
        var center = document.createElement("div");
        center.classList.add("game-center");
        this.canvas = document.createElement("canvas");
        this.canvas.classList.add("game");
        this.canvas.setAttribute("width",this.canvasWidth);
        this.canvas.setAttribute("height",this.canvasHeight);
        this.canvas.classList.add("game-hud");
        center.appendChild(this._createExit());
        center.appendChild(this.canvas);
        environment.getBasic().appendChild(center);
        this.ctx = this.canvas.getContext("2d"); // get ctx 2d
    }

    _addTank()
    {
        var tank = new Tank(false,this.player);
        tank._velocity = 3+(Math.random()*5)+this.speed;
        var x = this.canvasWidth;
        var y = Math.random()*this.canvasHeight;
        var vector = {x:0 - x, y: Math.random()*this.canvasHeight - y};
        tank.setCenterToRoot();
        tank._position.replace(0,Position.createRotateMatrix(180));
        tank._position.replace(1,Position.createMoveMatrix(x,y));
        tank.collision.attachList(gameData.getZones());
        gameData.getTanks().push(tank);
    }

    _gameLoop()
    {
        // drawing / apply transformation / process
        this.ctx.clearRect(0,0,this.canvasWidth,this.canvasHeight);
        for(var key in this.gameObjects)
        {
            var currentArray = this.gameObjects[key];
            for(var i = 0;i < currentArray.length;i++)
            {
                currentArray[i].gameLoopProcess(this.ctx);
                currentArray[i].events();
                if(currentArray[i].getActive() == false || currentArray[i].damaged)
                    currentArray.splice(i--,1);
            }
        }
        // adding tanks
        var date = Date.now();
        if(date - this._lastTime > this._intervalNewTank)
        {
            this._addTank();
            this._lastTime = Date.now();
        }
        if(date - this.speedTime > 5000)
        {
            this.speedTime = date;
            this.speed += 0.5;
            this._intervalNewTank -= 100;
        }
    }

    _createExit()
    {
        var exit = document.createElement("button");
        exit.classList.add("black");
        exit.textContent = "EXIT";
        exit.addEventListener("click",()=>{
            window.clearInterval(gameData.module.loop);
            this.end();
        })
        return exit;
    }
}


