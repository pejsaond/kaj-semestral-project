class Welcome
{
    constructor()
    {

    }

    build(environment)
    {
        this.element = environment.getBasic();
        var welcome =  `<h1 id="title-tankodrom">TANKODROM</h1>
                            <form class="windowContainer">
                                <label for="nickname">NICKNAME</label>
                                <input id="welcome-nickname" class="black" type="text" autofocus>
                                <input class="black" id="welcome-button" type="button" value="Play">
                                <input class="black" id="scoreboard-button" type="button" value="ScoreBoard">
                            </form>`;
        this.element.insertAdjacentHTML('beforeend',welcome);
        var buttonPlay = this.element.querySelector("#welcome-button");
        var buttonBoard = this.element.querySelector("#scoreboard-button");
        var text = this.element.querySelector("#welcome-nickname");
        text.classList.add("red-bottom");
        
        var startGame = function(event){
            var exists = this._alreadyExists(text.value);
            if(!exists && text.value != "")
            {
                environment.setModule(new Game());
                environment.clear();
                environment.build();
                gameData.setNickname(text.value);
            }
        }.bind(this);

        var scoreBoard = function(event){
            this.element.appendChild(this._scoreBoard());
        }.bind(this);

        buttonBoard.addEventListener("click",scoreBoard);
        buttonPlay.addEventListener("click",startGame);
        text.addEventListener("keyup",(e)=>{
            if(this._alreadyExists(e.target.value) || e.target.value == "")
            {
                text.classList.add("red-bottom");
                text.classList.remove("green-bottom");
            }
            else
            {
                text.classList.remove("red-bottom");
                text.classList.add("green-bottom");
            }
        });
    }

    exec(environment)
    {
        //for caching images to globalCache (js has garbage collector)
        new Cannon(false,gameData);
        new Zone();
        new Tank(false,this.player);
    }

    _scoreBoard()
    {
        var div = document.createElement("div");
        div.classList.add("windowContainer");
        div.setAttribute("id","scoreboard");
        var tableFixHead = document.createElement("div");
        tableFixHead.classList.add("tableFixHead");
        var table = document.createElement("table");
        table.innerHTML = this._tableData();
        tableFixHead.appendChild(table);
        div.appendChild(tableFixHead);
        var back = document.createElement("button");
        back.textContent = "BACK";
        back.classList.add("black");
        back.addEventListener("click",() => {
            this.element.removeChild(div);
        })
        back.setAttribute("id","back");
        div.appendChild(back);
        var clear = document.createElement("button");
        clear.textContent = "CLEAR";
        clear.classList.add("black");
        clear.addEventListener("click",() => {
            localStorage.clear();
            table.innerHTML = this._tableData();
            
        })
        clear.setAttribute("id","clear");
        div.appendChild(clear);
        return div;
    }

    _tableData()
    {
        var table = "<thead> <tr> <th>Name</th> <th>Score</th> <th>Date</th> </tr> </thead>";
        for(let [key, value] of Object.entries(localStorage))
        {
            value = JSON.parse(value);
            if(value.nick != undefined)
                table += `<tr> <td>${key}</td> <td>${value.nick}</td> <td>${value.date}</td> </tr>`;
        }
        return table;
    }

    _alreadyExists(nick)
    {
        for(let [key, value] of Object.entries(localStorage)) 
        {
            if(key == nick)
                return true;
        }
        return false;
    }

    _ascii(e)
    {
        var code = e.keyCode;
        console.log("code: ",code);
        if((code > 64 && code < 91) || (code > 96 && code < 123))
            return e.key;
        return "";
    }
}

